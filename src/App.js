/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
// App.js
import 'react-native-gesture-handler';
import React from 'react';
import { useState } from 'react';
// import type {Node} from 'react';

import {SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Vibration,
  Share,
  Platform, 
  Alert, 
  Modal, 
  Pressable,
  TextInput} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();
function HomeScreen({navigation}) {
  return(
    <View style={{alignItems: 'center', justifyContent: 'center'}}>
      <Text>Bienvenue dans notre application</Text>
      <Button
        title="Médecin le plus proche de moi"
        onPress={() => navigation.navigate('Maps')}
      />
      <Button
        title="Rechercher un médecin"
        onPress={() => navigation.navigate('Recherche')}
      />
    </View>
  );
  }
  function DetailsScreen() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text>Details Screen</Text>
      </View>
    );
  }
  function RechercheScreen() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text>Recherche</Text>
      </View>
    );
  }

  const App = () => {
    return ( 
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home">
          <Stack.Screen name="Home" component={HomeScreen} />
          <Stack.Screen name="Maps" component={DetailsScreen} />
          <Stack.Screen name="Recherche" component={RechercheScreen} />
        </Stack.Navigator>
      </NavigationContainer>
  );
};
export default App;